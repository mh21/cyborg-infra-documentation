---
title: "Purpose of these pages"
linktitle: "Purpose"
description: >
  Trying to explain why having engineers writing documentation outside their
  team boundaries is a good idea
weight: 10
---

## Introduction

These documentation pages try to facilitate exchange across teams about common
problems and their solutions.

Given such problems, these pages aim to allow engineers to find (in increasing
order of preference)

- links to already implemented solutions to use as inspiration or for
  copy-and-pasting into their own projects
- in addition to the links, also more extensive documentation of the thinking
  behind the solutions
- best practice reference implementations and guidelines

In the short term, this should result in a public place where teams can

- find documentation to get started tackling a certain problem
- document their implementations *for others to see*,
  even if they are not sure that they are doing things completely right, or are
  potentially reinventing the wheel
- discuss implementations and arrive at better solutions together

Longer term, this should also lead to

- the proactive implementation of proper solutions, e.g. for security aspects,
  instead of waiting for the retroactive feedback from the security team

**It is explicitely not the point of these pages to get teams to agree on one
common solution!**

## Contents

The repositories contain documentation on how things are done by individual
teams. Unless explicitely marked as such, the documentation does not
necessarily represent a consensus across teams of what constitutes best
practice.
