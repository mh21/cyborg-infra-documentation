---
title: Open Source Documentation
linkTitle: Documentation
description: >
  How to have open source service documentation while still providing internal
  details necessary for operation teams
---

## Problem

The [Open Source requirements for Services] of the [Operate First]
community are defined as:

> - All the service's code and assets necessary to operate the service are shared
>   under an open source license, and publicly accessible.
> - A public contributor can use the same workflow as a typical team member to
>   make a change to the service.

When applied to documentation, these result in the following requirements:

- documentation needed to operate the service needs to be shared publicly, e.g.
  via a public documentation repository
- the public documentation repository needs to be the main documentation
  repository for a project, i.e. the one team members typically use

For an active service, this documentation is also used during daily operation.
To be truly useful for that purpose, the documentation needs to contain
internal details that should not be present on the public repository.

## Solutions

Two different approaches are currently in use:

1. The documentation is split across two separate repositories. Optionally,
   both repositories get publish separately via a site generator like [Antora],
   [Hugo] or [MkDocs] (`split-repo`).
1. Nearly all of the documentation is present in the public repository.
   Internal details are contained only in the internal repository. From these
   repositories, two versions are published: a public version only containing
   information from the public repository, and an internal version containing
   information from both the public and internal repositories (`overlay`).

Of these approaches, the second (`overlay`) is preferred as it simplifies
daily operations.

| Team          | Approach                 |
|---------------|--------------------------|
| Cyborg        | [`overlay`][cyborg-docs] |
| AMUSE         |                          |
| Anaconda      |                          |
| Automotive    |                          |
| CKI           | [`overlay`][cyborg-docs] |
| Cockpit       |                          |
| Copr/CPT      | `split-repo`             |
| Image Builder | `split-repo`             |
| OSCI          |                          |
| Packit        |                          |
| Testing Farm  | `split-repo`             |

[Operate First]: https://www.operate-first.cloud/
[Open Source requirements for Services]: https://www.operate-first.cloud/community/open-source-services.html

[MkDocs]: https://www.mkdocs.org/
[Hugo]: https://gohugo.io/
[Antora]: https://antora.org/

[cyborg-docs]: https://cyborg-infra.gitlab.io/documentation/general/contributing/
